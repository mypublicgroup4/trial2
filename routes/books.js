const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

//to add a book
router.post('/', (request, response) => {
  const { book_title, publisher_name, author_name } = request.body

  const statement = `insert into book(book_title,publisher_name,author_name)
   values(?,?,?)`

  db.pool.query(
    statement,
    [book_title, publisher_name, author_name],
    (error, data) => {
      response.send(utils.createResult(error, data))
    }
  )
})

//to show book
router.get('/', (request, response) => {
  const statement = `select book_id,book_title,publisher_name,author_name from book`

  db.pool.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

//to delete book
router.delete('/:bookid', (request, response) => {
  const { bookid } = request.params
  console.log(bookid)
  const statement = `delete from book where book_id =?`

  db.pool.query(statement, [bookid], (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

module.exports = router
