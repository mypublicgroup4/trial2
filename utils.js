const createResult = (error, data) => {
  const result = {}
  if (error) {
    result['status'] = 'failed'
    result['error'] = error
  } else {
    result['status'] = 'success'
    result['data'] = data
  }
  return result
}

const createError = (error) => {
  return { status: 'failed', error }
}

const createSuccess = (data) => {
  return { status: 'success', data }
}

module.exports = { createResult, createError, createSuccess }
